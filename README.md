# A Docker Image for Doing Machine Learning Stuff With Python and Jupyter

## Building the Image from the Dockerfile

Run the following command while located on the Dockerfile's parent directory to build the image:

```bash
docker build -t ml-notebook:1.0 -t ml-notebook:latest ml-notebook
```

As can be seen from the Dockerfile, the image will install the latest Ubuntu version, along with Anaconda (the Python 2.7 version) and Jupyterhub. A default user named `jupyter` (with password `jupyter`) is also created. Port `8000`is exposed in order to interact with Jupyterhub and create notebooks.

## Running Containers

We recommend running containers with a mounted volume (in which you can, for instance, store your notebooks and data). You can run such a container by using a command similar to the following one:

```bash
docker run -d -P --name <container_name> -v /home/<username>/ml-data/:/home/jupyter/ml-data/ ml-notebook
```

## Interacting with containers

If you need to interact with the container (for example, if you wish to create new users), you can always run a shell command line by typing:

```bash
docker exec -ti <container_name> bash
```
